from django.contrib import admin

# Register your models here.
from .models import BinVO, Shoe


@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = ["import_href", "closet_name", "bin_number", "bin_size"]


@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = ["id", "manufacturer", "model_name", "color", "picture_url", "bin"]
