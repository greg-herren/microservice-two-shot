from django.urls import path

from .api_views import api_shoes, api_shoe_details

urlpatterns = [
    path("shoes/", api_shoes, name="api_shoes"),
    path("shoes/<int:id>/", api_shoe_details, name="delete_shoe"),
]
