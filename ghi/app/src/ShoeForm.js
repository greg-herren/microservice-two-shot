import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import loadData from './index';


function ShoeForm(props) {
    const navigate = useNavigate()

    const [bins, setBins] = useState([]);

    const [formData, setFormData] = useState({
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: "",
    })


    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleShoeSubmit = async (event) => {
        event.preventDefault();

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                manufacturer: "",
                model_name: "",
                color: "",
                picture_url: "",
                bin: "",
            })
            loadData();
            navigate("/shoes")
        }

    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add more shoes</h1>
                <form onSubmit={handleShoeSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input value={formData.manufacturer} onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.model_name} onChange={handleFormChange} placeholder="Model" required type="text" name="model_name" id="model" className="form-control" />
                    <label htmlFor="model">Model</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select value={formData.bin} onChange={handleFormChange} required id="bin" name="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                    Bin number {bin.bin_number} in {bin.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>

  )
}

export default ShoeForm;
