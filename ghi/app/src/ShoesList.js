import { Link } from "react-router-dom";


async function deleteHandler(id) {
    const deleteUrl = `http://localhost:8080/api/shoes/${id}`;
    const fetchConfig = {
        method: "delete",
        body: null,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(deleteUrl, fetchConfig);
    if (response.ok) {
        window.location.reload(false);
    }
}


function ShoesList(props) {
    if (!props.shoes || props.shoes.length == 0) {
        return (
            <>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Closet Name</th>
                        <th>Bin Number</th>
                    </tr>
                    </thead>
                </table>
                <h4 className="text-center">You don't have any shoes!</h4>
                <Link to="/shoes/new">
                    <button type="button" className="btn btn-primary">
                        Add new shoes
                    </button>
                </Link>
            </>
        );
    } else {
        return (
            <>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Closet Name</th>
                        <th>Bin Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.shoes.map((shoe, idx) => {
                        return (
                        <tr key={idx}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td>
                                <img src={`${shoe.picture_url}`} width="50" alt={`${shoe.color} ${shoe.model_name} by ${shoe.manufacturer}`}/>
                            </td>
                            <td>{ shoe.bin.closet_name }</td>
                            <td>{ shoe.bin.bin_number }</td>
                            <td>
                                <button type="button" onClick={() => deleteHandler(shoe.id)} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
                <Link to="/shoes/new">
                    <button type="button" className="btn btn-primary">
                        Add new shoes
                    </button>
                </Link>
            </>
        );
    }
    console.log(props.shoes);
    // return (
    // <>
    //     <table className="table table-striped">
    //         <thead>
    //         <tr>
    //             <th>Manufacturer</th>
    //             <th>Model</th>
    //             <th>Color</th>
    //             <th>Picture</th>
    //             <th>Closet Name</th>
    //             <th>Bin Number</th>
    //         </tr>
    //         </thead>
    //         <tbody>
    //         {props.shoes.map((shoe, idx) => {
    //             return (
    //             <tr key={idx}>
    //                 <td>{ shoe.manufacturer }</td>
    //                 <td>{ shoe.model_name }</td>
    //                 <td>{ shoe.color }</td>
    //                 <td>
    //                     <img src={`${shoe.picture_url}`} width="50" alt={`${shoe.color} ${shoe.model_name} by ${shoe.manufacturer}`}/>
    //                 </td>
    //                 <td>{ shoe.bin.closet_name }</td>
    //                 <td>{ shoe.bin.bin_number }</td>
    //                 <td>
    //                     <button type="button" onClick={() => deleteHandler(shoe.id)} className="btn btn-danger">Delete</button>
    //                 </td>
    //             </tr>
    //             );
    //         })}
    //         </tbody>
    //     </table>
    //     <Link to="/shoes/new">
    //         <button type="button" className="btn btn-primary">
    //             Add new shoes
    //         </button>
    //     </Link>
    // </>
    // );
  }

  export default ShoesList;
