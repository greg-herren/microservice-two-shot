import { Link } from 'react-router-dom';
import React, { Component } from "react";



function HatsList(props) {
    async function handleDelete(id) {

        const hatUrl = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "DELETE",
            body: null,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            window.location.reload(true);
        }

    };
    return (
        <div>
          <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Click here to Create a hat</Link>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.name }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style }</td>
                            <td>{ hat.color }</td>
                            <td>
                                <img src={`${hat.picture_url}`} width="50" alt="Hat"/>
                            </td>
                            <td>{ hat.location.closet_name }</td>
                            <td>
                                <button onClick={() => handleDelete(hat.id)} className="btn btn-lg btn-outline-danger ml-4">Delete</button>
                            </td>
                        </tr>
                        );
                })}
            </tbody>
            </table>
        </div>

    );
}

export default HatsList;
