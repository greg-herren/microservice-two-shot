import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import loadData from './index';

function HatForm(props) {
    const navigate = useNavigate();

    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        setName(event.target.value);
    }
    const handleFabricChange = (event) => {
        setFabric(event.target.value);
    }
    const handleStyleChange = (event) => {
        setStyle(event.target.value);
    }
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    }
    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.fabric = fabric;
        data.style = style;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;


        const hatUrl = `http://localhost:8090/api/hats/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            alert("Succesfully created")

            setName('');
            setFabric('');
            setStyle('');
            setColor('');
            setPictureUrl('');
            setLocation('');

            loadData();
            navigate('/hats');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
        }
      }

    useEffect(() => {
    fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Hat</h1>
                        <form onSubmit={handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"></input>
                                <label htmlFor="presenter_email">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleStyleChange} value={style} placeholder="Style" type="text" name="style" id="style" className="form-control"></input>
                                <label htmlFor="company_name">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"></input>
                                <label htmlFor="title">Color</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Picture</label>
                                <textarea onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture" required type="file" name="picture_url" id="picture_url" className="form-control" rows="3"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {locations.map(location => {
                                            return (
                                                <option key={location.href} value={location.href}>
                                                    {location.closet_name}
                                                </option>
                                            );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
