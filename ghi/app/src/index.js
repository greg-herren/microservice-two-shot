import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadData() {
  const shoesUrl = "http://localhost:8080/api/shoes/";
  const hatsUrl = "http://localhost:8090/api/hats/";

  let combinedData = {}

  let shoesData = {}
  try {
    const shoesResponse = await fetch(shoesUrl);
    if (shoesResponse.ok) {
      shoesData = await shoesResponse.json();
    } else {
      console.error('error', shoesResponse);
    }
  } catch (error) {
    console.error(error);
  }

  for (let [key, value] of Object.entries(shoesData)) {
    combinedData[key] = value;
  }

  let hatsData = {}
  try {
    const hatsResponse = await fetch(hatsUrl);
    if (hatsResponse.ok) {
      hatsData = await hatsResponse.json();
    } else {
      console.error('error', hatsResponse);
    }
  } catch (error) {
    console.error(error);
  }

  for (let [key, value] of Object.entries(hatsData)) {
    combinedData[key] = value;
  }

  root.render(
          <React.StrictMode>
            <App shoes={combinedData.shoes} hats={combinedData.hats} />
          </React.StrictMode>
        );
}


loadData();


export default loadData;
