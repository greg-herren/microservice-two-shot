# Wardrobify

Team:

* Huilin Li - Hats
* Greg Herren - Shoes

## Design

## Shoes microservice

The shoes microservice has two models:
- Shoe
- BinVO

The Shoe model has four fields: manufacturer, model_name, color, and picture_url amd a foreign key to the BinVO.

The BinVO model is a value object. There is a poller that runs every 60 seconds hitting the bins route of the wardrobe-api to ensure that the bin value-objects accurately reflect the wardrobe-api bins.

## Hats microservice

Hat microservice:
Models:
1. Hat   2. LocationVO;
Components:
1. HatsList 2. HatForm

Models and components they all fetch location data from wardrobe microservice.
